import Swiper from 'swiper';
import {Autoplay} from 'swiper/js/swiper.esm';

$(document).ready(function () {
    let iconsSlider = $('.JS--news-slider');

    if (iconsSlider.length !== 0) {
        var iconsSwiper = new Swiper((iconsSlider), {
            navigation: {
                nextEl: ".JS--news-slider-next",
                prevEl: ".JS--news-slider-prev",
            },
            breakpoints: {
                320: {
                    slidesPerView: 1,
                    spaceBetween: 30
                },
                768: {
                    slidesPerView:2,
                    spaceBetween: 40
                },
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 60
                },
            }
        })
    }
});
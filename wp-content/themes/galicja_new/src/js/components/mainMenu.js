$(document).ready(function () {
        $("header ul a").on('click', function(event) {
            if (this.hash !== "") {
                var hash = this.hash;

                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 2000, function() {
                    window.location.hash = hash;
                });
                return false;
            }
        });


    $(window).bind('scroll', function (e) {
        parallaxScroll();
    });

    function parallaxScroll() {
        var scrolled = $(window).scrollTop();
        $('.layer-1').css('top', (400 - (scrolled * .8)) + 'px')
    }

    $('.JS--toggle').click(function (e) {
        $('.JS--mob-menu').toggleClass('show-menu');
        $(this).toggleClass('close-menu');
    });

    $('.JS--mob-menu ul a' ).click(function (e) {
        $('.JS--mob-menu').toggleClass('show-menu');
        $('.JS--toggle').toggleClass('close-menu');
    });

});
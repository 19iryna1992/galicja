global.$ = global.jQuery = require('jquery');
require('stellar.js');
require('./components/mainMenu');
require('./components/sliders');
require('./components/parallax');
require('./components/gallery');

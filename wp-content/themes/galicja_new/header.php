<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package {{theme-domain}}
 */

if (!defined('ABSPATH')) exit;

$global_tracking_body = (class_exists('ACF')) ? get_field('global_tracking_body', 'options') : null;

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php
# Global / Page Tracking
if ($global_tracking_body) : echo $global_tracking_body; endif;
?>

<div id="page" class="site">

    <header class="o-header">
        <div class="container">
            <div class="row position-relative justify-content-end justify-content-md-center">
                <div class="o-header__logo">
                    <?= get_custom_logo(); ?>
                </div>
                <div class="d-none d-md-block col-auto">
                    <?php get_template_part('template-parts/organisms/header/main-menu'); ?>
                </div>
                <div class="col-auto d-md-none">
                    <div class="JS--toggle o-header__toggle-menu open-menu"></div>
                </div>
            </div>
        </div>
    </header>
    <div class="JS--mob-menu o-mobile-menu d-md-none">
        <?php get_template_part('template-parts/organisms/header/main-menu'); ?>
    </div>

    <div id="content" class="site-content">

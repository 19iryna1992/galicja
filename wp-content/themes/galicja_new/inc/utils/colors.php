<?php

if (!defined('ABSPATH')) exit;

/**
 *  galicja_get_bg_color_class()
 *
 *  @param   string $color Color
 *  @return  string
 *  @example galicja_get_bg_color_class($color)
 */
function galicja_get_bg_color_class($color = null)
{
    if(!$color) return '';
    $bg_colors = [
        'bg-white' => '',
        'bg-grey-light' => 'u-bg-grey-light',
        'bg-grey-dark' => 'u-bg-grey-dark',
        'bg-orange' => 'u-bg-orange',
    ];
    return (isset($bg_colors[$color])) ? $bg_colors[$color] : '';
}

/**
 *  galicja_bg_color()
 *  echo bg color property for element
 * 
 *  @param   string $color Color
 *  @return  string
 *  @example galicja_bg_color($color)
 */
function galicja_the_bg_color($color = null)
{
    echo ($color) ? "style='background-color: {$color}'" : "";
}

/**
 *  galicja_hex_to_rgba()
 *  Convert color format from hex to rgba
 *
 *  @param   string $color Color
 *  @param   string $opacity Opacity
 *  @return  string
 *  @example galicja_hex_to_rgba($color, $opacity)
 */
function galicja_hex_to_rgba($color = null, $opacity = null)
{
    if(!$color || !$opacity) return '';
    list($r, $g, $b) = sscanf($color, "#%02x%02x%02x");
    return sprintf('rgba(%d, %d, %d, %F)', $r, $g, $b, $opacity);

}
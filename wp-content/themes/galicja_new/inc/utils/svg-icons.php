<?php

if (!defined('ABSPATH')) exit;

/** *
 *
 * Function for getting svg icons
 *
 */

function get_map_icon()
{
    return  '<svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M13 2.16663C8.80752 2.16663 5.41669 5.55746 5.41669 9.74996C5.41669 15.4375 13 23.8333 13 23.8333C13 23.8333 20.5834 15.4375 20.5834 9.74996C20.5834 5.55746 17.1925 2.16663 13 2.16663ZM7.58338 9.74996C7.58338 6.75996 10.01 4.3333 13 4.3333C15.99 4.3333 18.4167 6.75996 18.4167 9.74996C18.4167 12.87 15.2967 17.5391 13 20.4533C10.7467 17.5608 7.58338 12.8375 7.58338 9.74996ZM10.2917 9.74996C10.2917 8.25419 11.5043 7.04163 13 7.04163C13.9676 7.04163 14.8617 7.55783 15.3455 8.39579C15.8293 9.23375 15.8293 10.2662 15.3455 11.1041C14.8617 11.9421 13.9676 12.4583 13 12.4583C11.5043 12.4583 10.2917 11.2457 10.2917 9.74996Z" fill="white"/>
</svg>';

}

function get_time_icon()
{
    return  '<svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M11 20.1666C16.0626 20.1666 20.1667 16.0625 20.1667 10.9999C20.1667 5.93731 16.0626 1.83325 11 1.83325C5.9374 1.83325 1.83334 5.93731 1.83334 10.9999C1.83334 16.0625 5.9374 20.1666 11 20.1666Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M11 5.5V11L14.6667 12.8333" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
</svg>';

}

function get_mail_icon()
{
    return  '<svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M21.6667 4.33325H4.33335C3.14169 4.33325 2.17752 5.30825 2.17752 6.49992L2.16669 19.4999C2.16669 20.6916 3.14169 21.6666 4.33335 21.6666H21.6667C22.8584 21.6666 23.8334 20.6916 23.8334 19.4999V6.49992C23.8334 5.30825 22.8584 4.33325 21.6667 4.33325ZM4.33338 8.66658L13 14.0832L21.6667 8.66658V19.4999H4.33338V8.66658ZM4.33338 6.49992L13 11.9166L21.6667 6.49992H4.33338Z" fill="white"/>
</svg>';

}

function get_phone_icon()
{
    return  '<svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M4.33333 3.25H8.125C8.72083 3.25 9.20833 3.7375 9.20833 4.33333C9.20833 5.6875 9.425 6.9875 9.82583 8.20083C9.945 8.58 9.85833 9.0025 9.555 9.30583L7.17167 11.6892C8.73167 14.755 11.245 17.2575 14.3108 18.8283L16.6942 16.445C16.9108 16.2392 17.1817 16.1308 17.4633 16.1308C17.5717 16.1308 17.6908 16.1417 17.7992 16.185C19.0125 16.5858 20.3233 16.8025 21.6667 16.8025C22.2625 16.8025 22.75 17.29 22.75 17.8858V21.6667C22.75 22.2625 22.2625 22.75 21.6667 22.75C11.4942 22.75 3.25 14.5058 3.25 4.33333C3.25 3.7375 3.7375 3.25 4.33333 3.25ZM7.08506 5.41667C7.15006 6.38084 7.31256 7.32334 7.57256 8.22251L6.27256 9.52251C5.82839 8.22251 5.54672 6.84667 5.44922 5.41667H7.08506ZM17.7667 18.4383C18.6875 18.6983 19.63 18.8608 20.5834 18.9258V20.54C19.1534 20.4425 17.7775 20.1608 16.4667 19.7275L17.7667 18.4383Z" fill="white"/>
</svg>';

}

function get_slider_prew_icon()
{
    return  '<svg width="23" height="39" viewBox="0 0 23 39" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M21 37L2 19.5L21 2" stroke="#954949" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"/>
</svg>';

}

function get_slider_next_icon()
{
    return  '<svg width="23" height="39" viewBox="0 0 23 39" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M2 2L21 19.5L2 37" stroke="#954949" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"/>
</svg>';

}

function get_close_icon()
{
    return  '<svg width="26" height="24" viewBox="0 0 26 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M23.4355 1.99991L12.7179 11.6458L23.4355 21.2917" stroke="#954949" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M2 1.99991L12.7177 11.6458L2 21.2917" stroke="#954949" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"/>
</svg>';

}
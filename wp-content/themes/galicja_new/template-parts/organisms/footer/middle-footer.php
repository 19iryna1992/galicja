<?php

if (!defined('ABSPATH')) exit;

//VARS ACF
$logo = get_field('company_logo', 'option');
$address = get_field('company_address', 'option');
$tel = get_field('company_phone_quote', 'option');
$email = get_field('company_email_general', 'option');
$schedule = get_field('company_schedule', 'option');
$way = get_field('company_way', 'option');

?>
<div class="o-footer__middle">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-9 col-md-6">
                <?php if ($logo): ?>
                    <div class="o-footer__logo">
                        <img src="<?= $logo['url'] ?>" alt="<?= $logo['alt'] ?>">
                    </div>
                <?php endif; ?>
                <ul class="o-footer__items">
                    <?php if (have_rows('company_social_media', 'option')): ?>
                        <li class="o-footer__item o-footer__social-box">
                            <?php while (have_rows('company_social_media', 'option')): the_row();
                                $icon = get_sub_field('icon');
                                $link = get_sub_field('link');;
                                ?>
                                <?php if ($link) : ?>
                                    <a class="o-footer__social" href="<?php echo $link['url'] ?>">
                                        <?php if ($icon): ?>
                                            <span class="o-footer__icon">
                                            <img src="<?php echo $icon['url'] ?>" alt="<?php echo $icon['alt'] ?>">
                                        </span>
                                        <?php endif; ?>
                                        <span>
                                            <?php echo $link['title'] ?>
                                        </span>
                                    </a>
                                <?php endif ?>
                            <?php endwhile; ?>
                        </li>
                    <?php endif; ?>
                    <?php if ($address): ?>
                        <li class="o-footer__item o-footer__address">
                              <span class="o-footer__icon">
                                <?= get_map_icon() ?>
                            </span>
                            <?= $address ?>
                        </li>
                    <?php endif; ?>
                    <?php if ($tel): ?>
                        <li class="o-footer__item o-footer__tel">
                              <span class="o-footer__icon">
                                <?= get_phone_icon() ?>
                            </span>
                            <a href="tel:<?= preg_replace("/[^0-9]/", '', $tel); ?>"><?= $tel ?></a>
                        </li>
                    <?php endif; ?>
                    <?php if ($email): ?>
                        <li class="o-footer__item o-footer__mail">
                              <span class="o-footer__icon">
                                <?= get_mail_icon() ?>
                            </span>
                            <a href="mailto:<?= $email ?>"><?= $email ?></a>
                        </li>
                    <?php endif; ?>
                    <?php if ($schedule): ?>
                        <li class="o-footer__item o-footer__schedule">
                            <span class="o-footer__icon">
                                <?= get_time_icon() ?>
                            </span>
                            <?= $schedule ?>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="d-none d-md-block col-lg-6">
                <?php if ($way): ?>
                    <div class="o-footer__way">
                        <?= $way ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
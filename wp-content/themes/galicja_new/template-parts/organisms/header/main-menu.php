<?php

if (!defined('ABSPATH')) exit;

?>
<div class="c-main-menu">
    <?php wp_nav_menu(array(
        'theme_location' => 'primary-menu',
        'menu' => 'Main Menu',
        'container_id' => 'cssmenu',
        'menu_class' => 'top-menu',
    )); ?>
</div>
<?php
//ACF fields
$gallery = get_field('gallery_imgs');
$size = 'full';
?>
<?php get_template_part('template-parts/components/modal-window'); ?>
<section class="s-gallery">
    <div class="container">
        <?php if ($gallery): ?>
            <div class="row JS--gallery">
                <?php foreach ($gallery as $image_id): ?>
                    <?php echo wp_get_attachment_image($image_id, $size, "", array("class" => "JS--gallery-thumbnail s-gallery__img col-12 col-md-4 col-lg-3 ")); ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</section>

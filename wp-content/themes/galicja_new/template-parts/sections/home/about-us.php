<?php

//VARS
$img_bg = get_field('about_us_bg_img');
$title = get_field('about_us_title');
$description = get_field('about_us_description');
?>

<section id="about_us" class="s-about-us" data-stellar-background-ratio="0.5"
         style='background-image: url("<?= $img_bg ? $img_bg['url'] : '' ?>")'>
    <div class="s-about-us__content__wrap d-none d-lg-block" style="left: auto" data-stellar-ratio="1.2">
        <div class="s-about-us__content">
            <?php if ($title): ?>
                <h2 class="c-intro__title s-about-us__title">
                    <?= $title ?>
                </h2>
            <?php endif; ?>
            <?php if ($description): ?>
                <div class="c-intro__description s-about-us__description">
                    <?= $description ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>

<section class="s-about-us__content-mob d-lg-none">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="s-about-us__content">
                    <?php if ($title): ?>
                        <h2 class="c-intro__title s-about-us__title">
                            <?= $title ?>
                        </h2>
                    <?php endif; ?>
                    <?php if ($description): ?>
                        <div class="c-intro__description s-about-us__description">
                            <?= $description ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>


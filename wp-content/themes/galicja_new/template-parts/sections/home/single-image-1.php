<?php
$img_bg = get_field('single_image');
?>

<?php if ($img_bg): ?>
    <section class="s-single-img" data-stellar-background-ratio="0.1" style='background-image: url("<?=$img_bg['url'] ?>");'></section>
<?php endif; ?>
<?php
$section_title = get_field('news_section_title');

if (have_rows('repeater_news')): ?>
    <section id="news" class="s-last-posts">
        <div class="container">
            <div class="row justify-content-center">
                <?php if ($section_title): ?>
                    <div class="col-12">
                        <h2 class="c-intro__title s-last-posts__title">
                            <?= $section_title ?>
                        </h2>
                    </div>
                <?php endif; ?>
                <div class="col-11">
                    <div class="c-slider__slider JS--news-slider swiper-container">
                        <div class="swiper-wrapper">
                            <?php while (have_rows('repeater_news')) : the_row();
                                $title = get_sub_field('title_news');
                                $description = get_sub_field('description_news'); ?>

                                <div class="swiper-slide">
                                    <div class="c-post-card">
                                        <?php if ($title): ?>
                                            <h3 class="c-post-card__title">
                                                <?= $title ?>
                                            </h3>
                                        <?php endif; ?>
                                        <?php if ($description): ?>
                                            <div class="c-post-card__description">
                                                <?= $description ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                    <div class="swiper-button-prev JS--news-slider-prev"></div>
                    <div class="swiper-button-next JS--news-slider-next"></div>
                </div>
            </div>
        </div>
    </section>
<?php endif;
wp_reset_postdata(); ?>

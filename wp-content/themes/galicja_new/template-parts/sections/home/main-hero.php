<?php
//VARS
$logo = get_field('main_hero_logo');

?>
<?php if ($logo): ?>
    <section class="s-main-hero">
        <div class="s-main-hero__decoration s-main-hero__decoration--left">
            <img src="/wp-content/themes/galicja/img/temporarily/left_hero_bg.svg" alt="hero-decor">
        </div>
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-12">
                    <div class="s-main-hero__logo">
                        <img src="<?php echo $logo['url'] ?>" alt="<?php echo $logo['alt'] ?>">
                    </div>
                </div>
            </div>
        </div>
        <div class="s-main-hero__decoration s-main-hero__decoration--right">
            <img src="/wp-content/themes/galicja/img/temporarily/right_hero_bg.svg" alt="hero-decor">
        </div>
    </section>
<?php endif; ?>
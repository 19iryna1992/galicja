<?php if (have_rows('services')): ?>
    <section id="services"  class="s-services">
        <img class="d-none d-lg-block" src="/wp-content/themes/galicja/img/temporarily/rings.svg" alt="rings">
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-12 col-lg-6">
                    <?php while (have_rows('services')): the_row();
                        //VARS
                        $title = get_sub_field('service_title');
                        $description = get_sub_field('service_description');
                        ?>
                        <div class="s-services__item">
                            <?php if ($title): ?>
                                <h2 class="c-intro__title s-services__item-title"><?= $title ?></h2>
                            <?php endif; ?>
                            <?php if ($description): ?>
                                <div class="s-services__item-description">
                                    <?= $description ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
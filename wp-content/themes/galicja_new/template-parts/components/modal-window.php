<div class="c-gallery-overlay JS--gallery-overlay"></div>
<div class="c-gallery-modal JS--gallery-modal">
    <img id="active-image" src="" alt="">
    <div id="left">
        <?php echo get_slider_prew_icon() ?>
    </div>
    <div id="btn-close">
        <?php echo get_close_icon() ?>
    </div>
    <div id="right">
        <?php echo get_slider_next_icon() ?>
    </div>
</div>


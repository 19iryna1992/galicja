<?php /* Template Name: Home */

if (!defined('ABSPATH')) exit;

get_header(); ?>

<main id="main" role="main" tabindex="-1">
    
    <?php get_template_part('template-parts/sections/home/main-hero'); ?>
    <?php get_template_part('template-parts/sections/home/about-us'); ?>
    <?php get_template_part('template-parts/sections/home/services'); ?>
    <?php get_template_part('template-parts/sections/home/single-image-1'); ?>
    <?php get_template_part('template-parts/sections/home/news'); ?>
    <?php get_template_part('template-parts/sections/home/single-image-2'); ?>


</main>

<?php get_footer(); ?>